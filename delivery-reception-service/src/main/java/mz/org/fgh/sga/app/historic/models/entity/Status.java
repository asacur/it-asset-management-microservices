package mz.org.fgh.sga.app.historic.models.entity;

public enum Status {
	
	FUNCIONAL("Funcional"),
	AVARIADO("Avariado"),
	ROUBADO("Roubado");
	
private final String descricao;
	
	Status(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
