package mz.org.fgh.sga.app.historic;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class FeignErrorDecoder implements ErrorDecoder {

    
    @Override
    public Exception decode(String methodKey, Response response) {
 
       
        switch (response.status()){
            case 400:
                 
            case 404:
            {
                   
              return new ResponseStatusException(HttpStatus.valueOf(response.status()), "Codigo do equipamento informático ou do utilizador não encontrado."); 
            }
            default:
                return new Exception(response.reason());
        } 
    }
    
}