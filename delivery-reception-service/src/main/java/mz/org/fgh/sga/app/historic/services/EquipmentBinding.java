package mz.org.fgh.sga.app.historic.services;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface EquipmentBinding {

    String EQUIPMENT = "equipmentChannel";

    @Input(EQUIPMENT)
    SubscribableChannel equipment();
    
 //   @Output("equipmentChannel")
   // MessageChannel historic();

}
