package mz.org.fgh.sga.app.historic.clients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import mz.org.fgh.sga.app.historic.models.entity.Equipment;

@FeignClient(name ="it-equipment-microservice")
public interface EquipmentFeignClient {

	@GetMapping(value = "/{id}")
	public Equipment findOne(@PathVariable("id") Long id);
	
	@GetMapping("/available")
	public List<Equipment> findAllAvailable();
	
	@PutMapping("/ativo/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void atualizarPropridadeAtivo(@PathVariable Long id);
}
