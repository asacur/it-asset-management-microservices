package mz.org.fgh.sga.app.historic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableEurekaClient
@EnableFeignClients
//@EnableBinding(HistoricBinding.class)
@EnableJpaAuditing
@SpringBootApplication
public class HistoricServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(HistoricServiceApplication.class, args);
	}

	@Bean
    public FeignErrorDecoder errorDecoder() {
        return new FeignErrorDecoder();
    }
}
