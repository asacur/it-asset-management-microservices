package mz.org.fgh.sga.app.historic.clients;

import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import mz.org.fgh.sga.app.historic.models.entity.User;

@FeignClient(name ="user-microservice")
public interface UserFeignClient {

	@GetMapping(value = "/{id}")
	public User findOne(@PathVariable("id") Long id);
	
	@GetMapping("/ids/{id}")
	public List<User>findByUserIdIn(@PathVariable List<Long> id);
}
