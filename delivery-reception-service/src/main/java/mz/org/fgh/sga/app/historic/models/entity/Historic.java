package mz.org.fgh.sga.app.historic.models.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;


@Entity(name = "historic")
@EntityListeners(AuditingEntityListener.class)
public class Historic {

	@Id
	@Column(name = "historic_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long historicId;

	private Long userId;

	@Transient
	private User user;

	@Transient
	private Equipment equipment;

	private Long equipmentId;

	@Column(name = "date_checkin")
	private Date dateCheckin;

	@Column(name = "date_checkout")
	private Date dateCheckout;

	@Column(name = "checkin_status")
	@Enumerated(EnumType.STRING)
	private Status checkinStatus;

	@CreatedDate
	@Column(name = "created_at")
	private Date createdAt;

	@LastModifiedDate
	@Column(name = "update_at")
	private Date updatedAt;

	private String notes;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Equipment getEquipment() {
		return equipment;
	}

	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}

	public Long getHistoricId() {
		return historicId;
	}

	public Date getDateCheckout() {
		return dateCheckout;
	}

	public void setDateCheckout(Date dateCheckout) {
		this.dateCheckout = dateCheckout;
	}

	public Status getCheckinStatus() {
		return checkinStatus;
	}

	public void setCheckinStatus(Status checkinStatus) {
		this.checkinStatus = checkinStatus;
	}

	public void setHistoricId(Long id) {
		this.historicId = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getEquipmentId() {
		return equipmentId;
	}

	public void setEquipmentId(Long equipmentId) {
		this.equipmentId = equipmentId;
	}

	public Date getDateCheckin() {
		return dateCheckin;
	}

	public void setDateCheckin(Date dateCheckin) {
		this.dateCheckin = dateCheckin;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
}
