package mz.org.fgh.sga.app.historic.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import mz.org.fgh.sga.app.historic.clients.EquipmentFeignClient;
import mz.org.fgh.sga.app.historic.clients.UserFeignClient;
import mz.org.fgh.sga.app.historic.models.entity.Equipment;
import mz.org.fgh.sga.app.historic.models.entity.Historic;
import mz.org.fgh.sga.app.historic.models.entity.User;
import mz.org.fgh.sga.app.historic.services.HistoricService;

@RestController
public class HistoricControllers {

	@Autowired
	private HistoricService service;

	@Autowired
	private EquipmentFeignClient equipamentoClient;

	@Autowired
	private UserFeignClient utilizadorClient;

	// private MessageChannel historicMessage;
	//
	// public HistoricControllers(HistoricBinding binding) {
	// historicMessage = binding.historic();
	// }

	@GetMapping("/")
	public ResponseEntity<?> findAll() {
		List<Historic> historic = (List<Historic>) service.findAll();

		return !historic.isEmpty() ? ResponseEntity.ok(historic) : ResponseEntity.notFound().build();
	}

	@GetMapping("/disponivel")
	public ResponseEntity<?> findAvailable() {

		List<Historic> historic = (List<Historic>) service.findByDateCheckoutIsNull();

		return !historic.isEmpty() ? ResponseEntity.ok(historic) : ResponseEntity.notFound().build();
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> findOne(@PathVariable Long id) {

		Optional<Historic> historic = service.findById(id);

		return historic.isPresent() ? ResponseEntity.ok(historic.get()) : ResponseEntity.notFound().build();
	}

	@GetMapping("/user/{id}")
	public ResponseEntity<?> findHistoricByUser(@PathVariable Long id) {

		User user = utilizadorClient.findOne(id);

		List<Historic> historic = (List<Historic>) service.findHistoricoByUserId(user.getUserId());
		historic = historic.stream().map(h -> {
			h.setUser(user);
			return h;
		}).collect(Collectors.toList());

		return !historic.isEmpty() ? ResponseEntity.ok(historic) : ResponseEntity.notFound().build();
	}

	@GetMapping("/equipment/{id}")
	public ResponseEntity<?> findHistoricByEquipment(@PathVariable Long id) {

		Equipment equipment = equipamentoClient.findOne(id);

		List<Historic> historic = (List<Historic>) service.findByEquipmentId(equipment.getAssetId());

		List<Long> idsUser = historic.stream().map(p -> p.getUserId()).collect(Collectors.toList());

		List<User> users = (List<User>) utilizadorClient.findByUserIdIn(idsUser);

		historic = historic.stream().map(h -> {
			users.forEach(u -> {
				if (u.getUserId() == h.getUserId()) {
					h.setUser(u);
				}
			});
			return h;
		}).collect(Collectors.toList());

		return !historic.isEmpty() ? ResponseEntity.ok(historic) : ResponseEntity.notFound().build();
	}

	@PostMapping("/")
	public ResponseEntity<?> save(@RequestBody Historic historic) {

		// List<Historic> historicIds = (List<Historic>)
		// service.findByDateCheckoutIsNull();

		// List<Long> busyHistoricIds = historicIds.stream().map( p ->
		// p.getEquipmentId()).collect(Collectors.toList());

		// if( !busyHistoricIds.contains(historic.getEquipmentId())) {

		// Equipment eq = equipamentoClient.findOne(historic.getEquipmentId());

		List<Equipment> equipment = (List<Equipment>) equipamentoClient.findAllAvailable();

		List<Long> availableIds = equipment.stream().map(e -> e.getAssetId()).collect(Collectors.toList());

		if (availableIds.contains(historic.getEquipmentId())) {

			Historic newHistorico = new Historic();
			newHistorico.setEquipmentId(historic.getEquipmentId());
			User ut = utilizadorClient.findOne(historic.getUserId());
			newHistorico.setUserId(ut.getUserId());
			newHistorico.setDateCheckout(new Date());

			Historic newHistoric = service.save(historic);
			// historicMessage.send(MessageBuilder.withPayload(historic.getEquipmentId()).build());
			equipamentoClient.atualizarPropridadeAtivo(historic.getEquipmentId());

			return ResponseEntity.status(HttpStatus.CREATED).body(newHistoric);
		}

		return ResponseEntity.badRequest().build();
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Historic historic) {

		Optional<Historic> historicOptional = service.findById(id);

		if (!historicOptional.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		Historic newHistoric = historicOptional.get();

		if (newHistoric.getEquipmentId() != historic.getEquipmentId()) {

			Equipment equipamento = equipamentoClient.findOne(historic.getEquipmentId());
			historic.setEquipmentId(equipamento.getAssetId());
		}

		if (historic.getCheckinStatus() == null && historic.getDateCheckin() == null) {

			BeanUtils.copyProperties(historic, newHistoric, "historicId", "createdAt");

			return ResponseEntity.status(HttpStatus.CREATED).body(service.save(newHistoric));
		}

		else if (historic.getCheckinStatus() != null && historic.getDateCheckin() != null) {

			BeanUtils.copyProperties(historic, newHistoric, "historicId", "createdAt");
			if (historic.getCheckinStatus().getDescricao().equals("Funcional")) {
				// historicMessage.send(MessageBuilder.withPayload(newHistoric.getEquipmentId()).build());
				equipamentoClient.atualizarPropridadeAtivo(historic.getEquipmentId());
			}
			return ResponseEntity.status(HttpStatus.CREATED).body(service.save(newHistoric));
		}

		return ResponseEntity.badRequest().build();

	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {

		service.deleteById(id);
		return ResponseEntity.noContent().build();
	}

}
