package mz.org.fgh.sga.app.historic.models.entity;

import java.util.Date;


public class Equipment{

	private Long assetId;
	private String assetName;
	private String assetTag;
	private Date createdAt;
	private Date updatedAt;
	private Boolean available;
	
	public Long getAssetId() {
		return assetId;
	}

	public void setAssetId(Long assetId) {
		this.assetId = assetId;
	}

	public String getAssetName() {
		return assetName;
	}

	public void setAssetName(String assetname) {
		this.assetName = assetname;
	}

	public String getAssetTag() {
		return assetTag;
	}

	public void setAssetTag(String assettag) {
		this.assetTag = assettag;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}


	
	
	
}