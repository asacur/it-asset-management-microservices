package mz.org.fgh.sga.app.historic.services;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface HistoricBinding {

	
	@Output("historicChannel")
    MessageChannel historic();
	

}
