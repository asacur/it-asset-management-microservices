package mz.org.fgh.sga.app.historic.services;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Service;
import mz.org.fgh.sga.app.historic.models.entity.Historic;
import mz.org.fgh.sga.app.historic.models.repository.HistoricRepository;

@EnableBinding(EquipmentBinding.class)
@Service
public class HistoricImpl implements HistoricService {

	@Autowired
	private HistoricRepository repository;

	

	@Override
	public Iterable<Historic> findAll() {

		return repository.findAll();
	}

	@Override
	public Optional<Historic> findById(Long id) {

		return repository.findById(id);
	}

	@Override
	public Historic save(Historic historico) {

		return repository.save(historico);

	}

	
	
	@StreamListener(target = EquipmentBinding.EQUIPMENT)
	@Override
	public void deleteById(Long id) {
		repository.deleteById(id);
	}

	@Override
	public Iterable<Historic> findHistoricoByUserId(Long userId) {
		// TODO Auto-generated method stub
		return repository.findByUserId(userId);
	}

	@Override

	public void handleDeletedProduct(Historic utilizador) {

		repository.deleteById(utilizador.getHistoricId());

	}

	@Override
	public Iterable<Historic> findByDateCheckoutIsNull() {

		return repository.findByDateCheckoutIsNull();
	}

	@Override
	public Iterable<Historic> findByEquipmentId(Long equipmentId) {
		
		return repository.findByEquipmentId(equipmentId);
	}



}
