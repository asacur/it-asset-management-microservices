package mz.org.fgh.sga.app.historic.services;

import java.util.Optional;
import mz.org.fgh.sga.app.historic.models.entity.Historic;

public interface HistoricService {

	public Iterable<Historic> findAll();
	
	public Optional<Historic> findById(Long id);
	
	public Historic save(Historic historic);
	
	public void deleteById(Long id);
	
	public Iterable<Historic> findHistoricoByUserId(Long userId);
	
	public Iterable<Historic> findByEquipmentId(Long equipmentId);
	
	public Iterable<Historic> findByDateCheckoutIsNull();
	
	public void handleDeletedProduct(Historic historic);
}
