package mz.org.fgh.sga.app.historic.models.repository;

import org.springframework.data.repository.CrudRepository;

import mz.org.fgh.sga.app.historic.models.entity.Historic;

public interface HistoricRepository extends CrudRepository<Historic, Long> {

	Iterable<Historic> findByDateCheckoutIsNull();
	Iterable<Historic> findByUserId(Long userId);
	Iterable<Historic> findByEquipmentId(Long equipmentId);
	Iterable<Historic> findByDateCheckoutIsNullAndDateCheckinIsNotNull();
}
