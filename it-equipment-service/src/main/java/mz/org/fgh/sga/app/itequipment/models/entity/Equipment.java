package mz.org.fgh.sga.app.itequipment.models.entity;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity(name = "equipament")
@EntityListeners(AuditingEntityListener.class)
public class Equipment{

	@Id
	@Column(name = "asset_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long assetId;
	
	@Column(nullable = false, name = "asset_name")
	private String assetName;
	
	@Column(nullable = false, name = "asset_tag")
	private String assetTag;
	
//	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	@Column(name = "created_at")
	private Date createdAt;

	@LastModifiedDate
	@Column(name = "update_at")
	private Date updatedAt;
	
	private Boolean available;
	
	@ManyToOne
	@JoinColumn(nullable = false, name = "category_id")
	private Category category;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "purchase_at")
	private Date purchaseAt;
	
	@Column(name = "purchase_cost")
	private BigDecimal purchaseCost;
	
	private String serial;
	
	private String notes;
	
	
	public Category getCategory() {
		return category;
	}


	public void setCategory(Category category) {
		this.category = category;
	}


	public Date getPurchaseAt() {
		return purchaseAt;
	}


	public void setPurchaseAt(Date purchaseAt) {
		this.purchaseAt = purchaseAt;
	}


	public BigDecimal getPurchaseCost() {
		return purchaseCost;
	}


	public void setPurchaseCost(BigDecimal purchaseCost) {
		this.purchaseCost = purchaseCost;
	}


	public String getSerial() {
		return serial;
	}


	public void setSerial(String serial) {
		this.serial = serial;
	}


	public String getNotes() {
		return notes;
	}


	public void setNotes(String notes) {
		this.notes = notes;
	}


	public Date getUpdatedAt() {
		return updatedAt;
	}


	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getAssetId() {
		return assetId;
	}

	public void setAssetId(Long assetId) {
		this.assetId = assetId;
	}

	public String getAssetName() {
		return assetName;
	}

	public void setAssetName(String assetname) {
		this.assetName = assetname;
	}

	public String getAssetTag() {
		return assetTag;
	}

	public void setAssetTag(String assettag) {
		this.assetTag = assettag;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}


	
	
	
}