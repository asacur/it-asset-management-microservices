package mz.org.fgh.sga.app.itequipment.services;

import java.util.List;
import java.util.Optional;

import mz.org.fgh.sga.app.itequipment.models.entity.Equipment;

public interface EquipmentService {

	public Iterable<Equipment> findAll();
	
	public Optional<Equipment> findById(Long id);
	
	public Equipment save(Equipment equipment);
	
	public void deleteById(Long id);
	
	public Iterable<Equipment> findByAssetIdNotIn(List<Long> id);
	
	public void updatePropertieActive( Long id);
	
	public Iterable<Equipment> findByAvailableTrue();
}
