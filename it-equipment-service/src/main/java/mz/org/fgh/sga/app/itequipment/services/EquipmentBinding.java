package mz.org.fgh.sga.app.itequipment.services;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface EquipmentBinding {
	 
	@Output("equipmentChannel")
	    MessageChannel equipment();
	  

}
