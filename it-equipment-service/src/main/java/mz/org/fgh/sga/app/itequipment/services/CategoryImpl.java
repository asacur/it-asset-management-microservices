package mz.org.fgh.sga.app.itequipment.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mz.org.fgh.sga.app.itequipment.models.entity.Category;
import mz.org.fgh.sga.app.itequipment.models.repository.CategoryRepository;

@Service
public class CategoryImpl implements CategoryService {

	@Autowired
	private CategoryRepository repository;

	@Override
	public Iterable<Category> findAll() {

		return repository.findAll();
	}

	@Override
	public Optional<Category> findById(Long id) {

		return repository.findById(id);
	}

	@Override
	public Category save(Category category) {

		return repository.save(category);
	}

	@Override
	public void deleteById(Long id) {

		repository.deleteById(id);

	}

}
