package mz.org.fgh.sga.app.itequipment.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import mz.org.fgh.sga.app.itequipment.models.entity.Equipment;
import mz.org.fgh.sga.app.itequipment.services.CategoryService;
import mz.org.fgh.sga.app.itequipment.services.EquipmentService;

@RestController
public class EquipmentControllers {

	@Autowired
	private EquipmentService service;
	@Autowired
	private CategoryService categoryService;

	//private MessageChannel equipment;

	//public EquipmentControllers(EquipmentBinding binding) {
		//equipment = binding.equipment();
	//}

	@GetMapping("/")
	public ResponseEntity<?> findAll() {

		List<Equipment> equipment = (List<Equipment>) service.findAll();

		return !equipment.isEmpty() ? ResponseEntity.ok(equipment) : ResponseEntity.notFound().build();
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> findOne(@PathVariable Long id) {

		Optional<Equipment> equipment = service.findById(id);

		return equipment.isPresent() ? ResponseEntity.ok(equipment.get()) : ResponseEntity.notFound().build();
	}
	
	@GetMapping("/available")
	public ResponseEntity<?> findAllAvailable() {

		List<Equipment> equipment = (List<Equipment>) service.findByAvailableTrue();

		return !equipment.isEmpty() ? ResponseEntity.ok(equipment) : ResponseEntity.notFound().build();
	}

	@PostMapping("/")
	public ResponseEntity<?> save(@RequestBody Equipment equipment) {

		if (categoryService.findById(equipment.getCategory().getCategoryId()).isPresent()) {
		
			 service.save(equipment);
			return ResponseEntity.status(HttpStatus.CREATED).body(equipment);
		}
	
		
      return ResponseEntity.notFound().build();
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Equipment equipament) {

		Optional<Equipment> equipamentOptional = service.findById(id);

		if (!equipamentOptional.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		Equipment newEquipament = equipamentOptional.get();

		BeanUtils.copyProperties(equipament, newEquipament, "assetId", "createdAt");

		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(newEquipament));
	}
	
	@PutMapping("/ativo/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void atualizarPropriedadeAtivo(@PathVariable Long id) {
		service.updatePropertieActive(id);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> save(@PathVariable Long id) {

		Optional<Equipment> equipamentOptional = service.findById(id);
		if (equipamentOptional.isPresent()) {
			service.deleteById(id);
		//	equipment.send(MessageBuilder.withPayload(id).build());
			return ResponseEntity.status(HttpStatus.CREATED).body(true);
		}
		return ResponseEntity.notFound().build();

	}

}
