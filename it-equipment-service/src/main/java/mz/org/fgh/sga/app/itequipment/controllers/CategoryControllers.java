package mz.org.fgh.sga.app.itequipment.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mz.org.fgh.sga.app.itequipment.models.entity.Category;
import mz.org.fgh.sga.app.itequipment.services.CategoryService;

@RequestMapping("/category")
@RestController
public class CategoryControllers {

	@Autowired
	private CategoryService service;


	@GetMapping("/")
	public ResponseEntity<?> findAll() {

		List<Category> categories = (List<Category>) service.findAll();

		return !categories.isEmpty() ? ResponseEntity.ok(categories) : ResponseEntity.notFound().build();
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> findOne(@PathVariable Long id) {

		Optional<Category> category = service.findById(id);

		return category.isPresent() ? ResponseEntity.ok(category.get()) : ResponseEntity.notFound().build();
	}

	@PostMapping("/")
	public ResponseEntity<?> save(@RequestBody Category category) {

		Category newCategory = service.save(category);

		return ResponseEntity.status(HttpStatus.CREATED).body(newCategory);
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Category category) {

		Optional<Category> categoryOptional = service.findById(id);

		if (!categoryOptional.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		Category newCategory = categoryOptional.get();

		BeanUtils.copyProperties(category, newCategory, "categoryId");

		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(newCategory));
	}
	

	@DeleteMapping("/{id}")
	public ResponseEntity<?> save(@PathVariable Long id) {

		Optional<Category> categoryOptional = service.findById(id);
		if (categoryOptional.isPresent()) {
			service.deleteById(id);
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.notFound().build();

	}

}
