package mz.org.fgh.sga.app.itequipment.services;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface HistoricBinding {

	
	  String HISTORIC = "historicChannel";
	  
	  @Input(HISTORIC)
	    SubscribableChannel historic();
	 
	
}
