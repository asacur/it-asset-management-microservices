package mz.org.fgh.sga.app.itequipment.services;

import java.util.Optional;

import mz.org.fgh.sga.app.itequipment.models.entity.Category;

public interface CategoryService {

	public Iterable<Category> findAll();
	
	public Optional<Category> findById(Long id);
	
	public Category save(Category category);
	
	public void deleteById(Long id);
	
}
