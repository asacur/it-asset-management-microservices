package mz.org.fgh.sga.app.itequipment.models.repository;

import org.springframework.data.repository.CrudRepository;

import mz.org.fgh.sga.app.itequipment.models.entity.Category;

public interface CategoryRepository extends CrudRepository<Category, Long> {


}
