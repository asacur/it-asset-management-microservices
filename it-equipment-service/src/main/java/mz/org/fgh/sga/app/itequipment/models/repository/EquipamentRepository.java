package mz.org.fgh.sga.app.itequipment.models.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import mz.org.fgh.sga.app.itequipment.models.entity.Equipment;

public interface EquipamentRepository extends CrudRepository<Equipment, Long> {

	Iterable<Equipment> findByAssetIdNotIn(List<Long> id);
	
	//@Query("select e from Equipment e WHERE e.available = TRUE ")
	Iterable<Equipment> findByAvailableTrue();
}
