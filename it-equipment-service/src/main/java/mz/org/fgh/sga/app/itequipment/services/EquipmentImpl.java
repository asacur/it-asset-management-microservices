package mz.org.fgh.sga.app.itequipment.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mz.org.fgh.sga.app.itequipment.models.entity.Equipment;
import mz.org.fgh.sga.app.itequipment.models.repository.EquipamentRepository;

//@EnableBinding(HistoricBinding.class)
@Service
public class EquipmentImpl implements EquipmentService {

	@Autowired
	private EquipamentRepository repository;

	@Override
	public Iterable<Equipment> findAll() {

		return repository.findAll();
	}

	@Override
	public Optional<Equipment> findById(Long id) {

		return repository.findById(id);
	}

	@Override
	public Equipment save(Equipment equipment) {

		return repository.save(equipment);
	}

	@Override
	public void deleteById(Long id) {

		repository.deleteById(id);

	}

	@Override
	public Iterable<Equipment> findByAssetIdNotIn(List<Long> id) {

		return repository.findByAssetIdNotIn(id);
	}

	//@StreamListener(target = HistoricBinding.HISTORIC)
	@Override
	public void updatePropertieActive(Long id) {
		
		Optional<Equipment> eqO = findById(id);
		
		Equipment eq = eqO.get();
		
		
		eq.setAvailable(!eq.getAvailable());
		
		repository.save(eq);
		
		
		
	}

	@Override
	public Iterable<Equipment> findByAvailableTrue() {
	
		return repository.findByAvailableTrue();
	}

}
