const fetch = require('node-fetch')
const equipmentURL = `http://localhost:7777`

const resolvers = {
  Query: {

   
    // verificando o status da resposta se for ok procede caso nao retorna um erro
    equipment: (parent, args) => {
      const { id } = args
      return fetch(`${equipmentURL}/${id}`).
        then(res => {
          console.log("response=", res.status);
          if (res.status == 200) {
            return res.json();
          } else
            return Promise.reject(new Error(`cannot found equipment for id  ${id}`))
        })

    },
    equipments: () => {
      return fetch(`${equipmentURL}/`).then(res => res.json())
    }

  },
  Mutation: {
    
    createEquipment: (parent, args) => {
      const equipment = {
        assetName: args.e.assetName,
        assetTag: args.e.assetTag,
        available: args.e.available,
        category: { categoryId: args.e.category.categoryId },
        purchaseAt: args.e.purchaseAt,
        purchaseCost: args.e.purchaseCost,
        serial: args.e.serial,
        notes: args.e.notes
      }
      return fetch(`${equipmentURL}/`, {
        method: 'POST',
        body: JSON.stringify(equipment),
        headers: { 'Content-Type': 'application/json' }
      }).then(res => res.json())
        .then(console.log(JSON.stringify(equipment)));
    },
    updateEquipment: (parent, args) => {
      const equipment = {
        assetName: args.e.assetName,
        assetTag: args.e.assetTag,
        available: args.e.available,
        category: { categoryId: args.e.category.categoryId },
        purchaseAt: args.e.purchaseAt,
        purchaseCost: args.e.purchaseCost,
        serial: args.e.serial,
        notes: args.e.notes
      }
      return fetch(`${equipmentURL}/${args.assetId}`, {
        method: 'PUT',
        body: JSON.stringify(equipment),
        headers: { 'Content-Type': 'application/json' }
      }).then(res => res.json())
        .then(console.log(JSON.stringify(equipment)));
    },
    deleteEquipment: (parent, args) => {
      const { assetId } = args
      return fetch(`${equipmentURL}/${assetId}`, {
        method: 'DELETE'
      }).then(res => res.json()).catch(error => console.log(error));
    }


  },



}

module.exports =resolvers




