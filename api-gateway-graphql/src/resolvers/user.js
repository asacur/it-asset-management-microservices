
const fetch = require('node-fetch')
const userURL = `http://localhost:8888`


const resolvers = {
  Query: {

    user: async (parent, args) => {
      const { id } = args
      return await fetch(`${userURL}/${id}`).then(res => res.json())
    },
    users: async () => {
      return await fetch(`${userURL}/`).then(res => res.json())
    }
  },
  Mutation: {
    createUser: (parent, args) => {
      const user = {
        name: args.user.name,
        username: args.user.username,
        password: args.user.password,
        //email: args.email,
        phoneNumber: args.user.phoneNumber
      }
      return fetch(`${userURL}/`, {
        method: 'POST',
        body: JSON.stringify(user),
        headers: { 'Content-Type': 'application/json' }
      }).then(res => res.json());
    },
    updateUser: (parent, args) => {
      const user = {
        name: args.name,
        username: args.username,
        password: args.password,
        //email: args.email,
        phoneNumber: args.phoneNumber
      }
      return fetch(`${userURL}/${args.userId}`, {
        method: 'PUT',
        body: JSON.stringify(user),
        headers: { 'Content-Type': 'application/json' }
      }).then(res => res.json());
    },
    deleteUser: (parent, args) => {
      const { id } = args
      return fetch(`${userURL}/${id}`, {
        method: 'DELETE'
      }).then(res => res.json()).catch(error => console.log(error));
    }
    },


  }





module.exports =resolvers




