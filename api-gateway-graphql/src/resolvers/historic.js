const fetch = require('node-fetch')
const historicURL = `http://localhost:6666`

const resolvers = {
  Query: {

    historic: (parent, args) => {
      const { id } = args
      return fetch(`${historicURL}/${id}`).then(res => res.json())
    },
    historicByUser: (parent, args) => {
      const { id } = args
      return fetch(`${historicURL}/user/${id}`).then(res => res.json())
    },
    historicByEquipment: (parent, args) => {
      const { id } = args
      return fetch(`${historicURL}/equipment/${id}`).then(res => res.json())
    },
    historics: () => {
      return fetch(`${historicURL}/`).then(res => res.json())
    },
  },
  Mutation: {

    createHistoric: (parent, args) => {
      const historic = {
        userId: args.h.userId,
        equipmentId: args.h.equipmentId,
        dateCheckout: args.h.dateCheckout,
        notes: args.h.notes
      }
      return fetch(`${historicURL}/`, {
        method: 'POST',
        body: JSON.stringify(historic),
        headers: { 'Content-Type': 'application/json' }
      }).then(res => res.json())
        .then(console.log(JSON.stringify(historic)));
    },
    updateHistoric: (parent, args) => {
      const historic = {
        userId: args.h.userId,
        equipmentId: args.h.equipmentId,
        dateCheckout: args.h.dateCheckout,
        notes: args.h.notes,
        dateCheckin: args.dateCheckin,
        checkinStatus: args.checkinStatus

      }
      return fetch(`${historicURL}/${args.historicId}`, {
        method: 'PUT',
        body: JSON.stringify(historic),
        headers: { 'Content-Type': 'application/json' }
      }).then(res => res.json())
        .then(console.log(JSON.stringify(historic)));
    },


  },



}

module.exports =resolvers




