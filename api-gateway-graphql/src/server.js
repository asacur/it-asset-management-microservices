const resolvers = require('./resolvers/index')
const { GraphQLServer } = require('graphql-yoga')

const server = new GraphQLServer({
    typeDefs: './src/schema/schema.graphql',
    resolvers,
  
  })
  
  server.start(() => console.log(`Server is running on http://localhost:4000`))