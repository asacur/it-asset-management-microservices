package mz.org.fgh.sga.app.users.services;

import java.util.List;
import java.util.Optional;

import mz.org.fgh.sga.app.users.models.entity.User;

public interface UserService {

	public Iterable<User> findAll();
	
	public Iterable<User> findByUserIdIn(List<Long> ids);
	
	public Optional<User> findById(Long id);
	
	public Optional<User> findByUsername(String username);
	
	public User save(User user);
	
	public void deleteById(Long id);
	
	public Boolean deleteUserById(Long id);
}
