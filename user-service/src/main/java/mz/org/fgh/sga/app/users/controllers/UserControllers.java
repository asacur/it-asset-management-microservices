package mz.org.fgh.sga.app.users.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import mz.org.fgh.sga.app.users.models.entity.User;
import mz.org.fgh.sga.app.users.services.UserService;

@RestController
public class UserControllers {

	@Autowired
	private UserService service;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@GetMapping("/")
	public ResponseEntity<?> findAll() {

		List<User> users = (List<User>) service.findAll();

		return !users.isEmpty() ? ResponseEntity.ok(users) : ResponseEntity.notFound().build();
	}
	
	
	@GetMapping("/ids/{id}")
	public ResponseEntity<?> findByUserIdIn(@PathVariable List<Long> id) {

		List<User> users = (List<User>) service.findByUserIdIn(id);

		return !users.isEmpty() ? ResponseEntity.ok(users) : ResponseEntity.notFound().build();
	}


	@GetMapping("/{id}")
	public ResponseEntity<?> findOne(@PathVariable Long id) {

		Optional<User> user = service.findById(id);

		return user.isPresent() ? ResponseEntity.ok(user.get()) : ResponseEntity.notFound().build();
	}
	
	@GetMapping("/user/{username}")
	public ResponseEntity<?> findByUsername(@PathVariable String username) {
		
		Optional<User> user = service.findByUsername(username);

		return user.isPresent() ? ResponseEntity.ok(user.get()) : ResponseEntity.notFound().build();
	}

	@PostMapping("/")
	public ResponseEntity<?> save(@RequestBody User user) {

		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		
		User newUser = service.save(user);

		return ResponseEntity.status(HttpStatus.CREATED).body(newUser);
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> update(@PathVariable Long id, @RequestBody User user) {

		Optional<User> userOptional = service.findById(id);

		if (!userOptional.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		User newUser = userOptional.get();

		BeanUtils.copyProperties(user, newUser, "userId", "createdAt");

		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(newUser));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteById(@PathVariable Long id) {
		Optional<User> user = service.findById(id);
		
		if (user.isPresent()) {
			service.deleteById(id);
			//service.deleteUserById(id);
			
			//return ResponseEntity.noContent().build();
			return ResponseEntity.status(HttpStatus.OK).body(true);
		}

		return ResponseEntity.notFound().build();
	}

}
