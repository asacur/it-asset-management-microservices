package mz.org.fgh.sga.app.users.models.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import mz.org.fgh.sga.app.users.models.entity.User;

public interface UserRepository extends PagingAndSortingRepository<User, Long> {

	public Optional<User> findByUsername(String username);

	@Modifying
	@Transactional
	@Query("DELETE FROM User u WHERE u.userId = ?1")
	default Boolean deleteUserById(Long userId) {
		return true;
	}

	Iterable<User> findByUserIdIn(List<Long> ids);
}
