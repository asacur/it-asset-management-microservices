package mz.org.fgh.sga.app.users.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mz.org.fgh.sga.app.users.models.entity.User;
import mz.org.fgh.sga.app.users.models.repository.UserRepository;

@Service
public class UserImpl implements UserService {

	@Autowired
	private UserRepository repository;
	
	@Override
	public Iterable<User> findAll() {

		return repository.findAll();
	}

	@Override
	public Optional<User> findById(Long id) {

		return repository.findById(id);
	}

	@Override
	public User save(User user) {
		
		return repository.save(user);
	}

	@Override
	public void deleteById(Long id) {

	
		repository.deleteById(id);
		
	}

	@Override
	public Boolean deleteUserById(Long id) {
		
		return repository.deleteUserById(id);
	}

	@Override
	public Iterable<User> findByUserIdIn(List<Long> ids) {

		return repository.findByUserIdIn(ids);
	}

	@Override
	public Optional<User> findByUsername(String username) {
		
		return repository.findByUsername(username);
	}



}
